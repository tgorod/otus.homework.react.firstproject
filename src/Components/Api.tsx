import axios from 'axios'

export const getAPI = async (url: string): Promise<any> =>{
    return await axios.get(url)
    .then ( (response) => {
        console.log(response)
        return {
            status: true,
            data: response.data
        }
    }).catch((error) =>{
        if (axios.isAxiosError(error)) {
            console.error("Axios error:", error.message);
            console.log(error);
            return {
                status: false,
                data: error.message
            }
          } else {
            console.error("General error:", error.message);
            console.log(error);
            return {
                status: false,
                data: error.response
            }
          }

    })
}