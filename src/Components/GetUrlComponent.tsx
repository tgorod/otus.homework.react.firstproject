import { Component, ReactNode } from "react";
import {Props} from './Props'


type MyState = { inputText: string };
export class GetUrlComponent extends Component<Props, MyState>{
    constructor(props: Props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {inputText: 'https://catfact.ninja/fact'};
      }
      
      handleChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.setState({inputText: e.target.value});
      }

    render(): ReactNode {
        const inputText = this.state.inputText;

        return <div>
             Введите строку поиска: 
               <input type='text' defaultValue={inputText} onChange={this.handleChange}></input>
               <button onClick={(e) => this.props.onButtonClick(inputText)}>Отправить</button>
        </div>;
    }
}