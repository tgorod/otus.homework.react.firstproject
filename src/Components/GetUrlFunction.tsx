import React, { useState } from 'react';
import {Props} from './Props'

export function GetUrlFunction(props: Props){
    const [inputText, setInputText] = useState('https://catfact.ninja/fact');

    const onBtnClick : React.MouseEventHandler = (e) => props.onButtonClick(inputText);
    
    const changeHandler = (e: React.FormEvent<HTMLInputElement>) => {
        setInputText(e.currentTarget.value);
        console.log('jjjjj');
        console.log(e.currentTarget.value);
    };

    return  <div>
        Введите строку поиска: 
        <input id='insertedText' type='text' defaultValue={inputText} onChange={changeHandler}></input>
        <button onClick={onBtnClick}>Отправить</button>
    </div>;
}
