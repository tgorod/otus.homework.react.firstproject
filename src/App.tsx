import React, { useState } from 'react';
import './App.css'
import { GetUrlFunction } from './Components/GetUrlFunction';
import { GetUrlComponent } from './Components/GetUrlComponent';
import { getAPI } from './Components/Api';


function App() {
  
  const [data, setData] = useState(['Пока ничего не получили']);
  const [status, setStatus] = useState(true);

  const getData = (url: string) => getAPI(url).then(
    (res) => {
        setData(res.data);
        setStatus(res.status);
     }
  )
  const onClick = (text : string | undefined)=>{
    if (text?.startsWith('http://') || text?.startsWith('https://')){
      getData(text);
    }else{
      setStatus(false);
      setData(['Некорректный url']);
    }
  }

  return (
    <div className="App">
      компонент-функция:
      <GetUrlFunction onButtonClick = {onClick} />
      компонент-класс:
      <GetUrlComponent onButtonClick = {onClick} />
      получили:
      <div>{status}</div>
      <div>{status ? 'trtrtr' : 'ffffffff'}</div>
      <div className={status ? 'blackText' : 'redText'}>{JSON.stringify(data)}</div>
      
    </div>
  );
}

export default App;
